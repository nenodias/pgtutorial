package db

import (
	"log"
	"time"

	pg "github.com/go-pg/pg"
	orm "github.com/go-pg/pg/orm"
)

type ProductItem struct {
	RefPointer int      `sql:"-"`
	tableName  struct{} `sql:"product_item_collection"`
	ID         int      `sql:"id,pk"`
	Name       string   `sql:"name,unique"`
	Desc       string   `sql:"desc"`
	Image      string   `sql:"image"`
	Price      float64  `sql:"price,type:real"`
	Features   struct {
		Name string
		Desc string
		Imp  int
	} `sql:"features,jsonb"`
	CreatedAt time.Time `sql:"created_at"`
	UpdatedAt time.Time `sql:"updated_at"`
	IsActive  bool      `sql:"is_active"`
}

func (pi *ProductItem) GetByDescOrName(db *pg.DB) (items []ProductItem, err error) {
	err = db.Model(&items).Column("name", "desc", "price").
		Where("name = ?0", pi.Name).
		WhereOr("price = ?0", pi.Price).
		Offset(1).
		Limit(3).
		Order("id desc").
		Select()
	if err != nil {
		log.Printf("Erro while selecting a Product Item by desc, Reason: %v\n", err)
		return nil, err
	}
	log.Printf("Total: %v\n", len(items))
	log.Printf("Get by id successful for %v\n", items)
	return items, nil
}

func (pi *ProductItem) GetById(db *pg.DB) error {
	//getErr := db.Select(pi)//Select byId
	getErr := db.Model(pi).Where("id = ?0", pi.ID).Select()
	if getErr != nil {
		log.Printf("Erro while selecting a Product Item, Reason: %v\n", getErr)
		return getErr
	}
	log.Printf("Get by id successful for %v\n", pi)
	return nil
}

func (pi *ProductItem) UpdatePrice(db *pg.DB) error {
	tx, txErr := db.Begin()
	if txErr != nil {
		log.Printf("Erro while open tx, Reason: %v\n", txErr)
		return txErr
	}
	Result, updateErr := tx.Model(pi).Set("price = ?price").Where("id = ?id").Update()
	if updateErr != nil {
		log.Printf("Erro while updating a Product Item Price, Reason: %v\n", updateErr)
		tx.Rollback()
		return updateErr
	}
	tx.Commit()
	log.Printf("Product %s was updated successfully, rows affected: %d", pi.Name, Result.RowsAffected())
	return nil
}

func (pi *ProductItem) DeleteItem(db *pg.DB) error {
	Result, deleteErr := db.Model(pi).Where("name = ?name").Delete()
	if deleteErr != nil {
		log.Printf("Erro while deleting a Product Item, Reason: %v\n", deleteErr)
		return deleteErr
	}
	log.Printf("Product %s was deleted successfully, rows affected: %d", pi.Name, Result.RowsAffected())
	return nil
}

func (pi *ProductItem) SaveAndReturn(db *pg.DB) (*ProductItem, error) {
	InsertResult, insertErr := db.Model(pi).Returning("*").Insert()
	if insertErr != nil {
		log.Printf("Error while inserting new item, Reason %v\n", insertErr)
		return nil, insertErr
	}
	log.Printf("ProductItem inserted successfully.\n")
	log.Printf("Rows affecteds: %v\n", InsertResult.RowsReturned())
	return pi, nil
}

func (pi *ProductItem) Save(db *pg.DB) error {
	insertErr := db.Insert(pi)
	if insertErr != nil {
		log.Printf("Error while inserting new item into DB, Reason: %v\n", insertErr)
		return insertErr
	}
	log.Printf("ProductItem %d - %s inserted successfully.\n", pi.ID, pi.Name)
	return nil
}

func (pi *ProductItem) SaveMultiples(db *pg.DB, items []interface{}) error {
	Result, insertErr := db.Model(items...).Insert()
	if insertErr != nil {
		log.Printf("Error while inserting bilk items, Reason: %v\n", insertErr)
		return insertErr
	}
	log.Printf("ProductItem bulk successfully.\n")
	log.Printf("Rows affecteds: %v\n", Result.RowsReturned())
	return nil
}

func CreateProdItemsTable(db *pg.DB) error {
	opts := &orm.CreateTableOptions{
		IfNotExists: true,
	}
	createErr := db.CreateTable(&ProductItem{}, opts)
	if createErr != nil {
		log.Printf("Error while creating table productItems, Reason: %v\n", createErr)
		return createErr
	}
	log.Printf("Sucessful created table productItems")
	return nil
}
