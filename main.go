package main

import (
	"log"
	"time"

	pg "github.com/go-pg/pg"
	db "github.com/you/pgtutorial/db"
)

func main() {
	pgDb := db.Connect()
	//SaveProduct(pgDb)
	//db.PlaceHolderDemo(pgDb)
	GetList(pgDb)
}

func SaveProduct(dbRef *pg.DB) {
	newPI1 := &db.ProductItem{
		Name:  "Product 8",
		Desc:  "Product 8 description",
		Image: "This is image path",
		Price: 12.25,
		Features: struct {
			Name string
			Desc string
			Imp  int
		}{
			Name: "F0",
			Desc: "F0 desc",
			Imp:  1,
		},
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		IsActive:  true,
	}
	newPI2 := &db.ProductItem{
		Name:  "Product 9",
		Desc:  "Product 9 description",
		Image: "This is image path",
		Price: 12.25,
		Features: struct {
			Name string
			Desc string
			Imp  int
		}{
			Name: "F0",
			Desc: "F0 desc",
			Imp:  1,
		},
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		IsActive:  true,
	}
	totalItems := []interface{}{newPI1, newPI2}
	err := newPI1.SaveMultiples(dbRef, totalItems)
	if err != nil {
		log.Printf("Error: %v\n", err)
	}
}

func Delete(dbRef *pg.DB) {
	newPi := &db.ProductItem{
		Name: "Product 3",
	}
	newPi.DeleteItem(dbRef)
}

func Update(dbRef *pg.DB) {
	newPi := &db.ProductItem{
		ID:    8,
		Price: 20,
	}
	newPi.UpdatePrice(dbRef)
}

func Get(dbRef *pg.DB) {
	newPi := &db.ProductItem{
		ID:   10,
		Name: "Product 5",
	}
	newPi.GetById(dbRef)
}

func GetList(dbRef *pg.DB) {
	newPi := &db.ProductItem{
		Name:  "Product 5",
		Price: 20,
	}
	items, _ := newPi.GetByDescOrName(dbRef)
	for index, i := range items {
		log.Print("Item(%d) \n\n%v\n", index, i)
	}
}
